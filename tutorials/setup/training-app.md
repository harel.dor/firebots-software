# Dev environment for training app

Currently these instructions are for Mac OS X only. Linux users should be able to follow along pretty easily, generally replacing `brew` with `apt-get`/`yum`/etc. Both Linux and OS X have the unix-nature.

## Install homebrew
Open Terminal.app. Download and install homebrew (instructions from http://brew.sh/):
```bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Make sure that you (1) follow all instructions and (2) do
```bash
brew doctor # check for postinstall problems
brew update # get notified of new packages
brew upgrade # download and install outdated packages
```

## Install git
```bash
brew install git
```

## Install rvm

```bash
\curl -sSL https://get.rvm.io | bash -s stable
source /etc/profile.d/rvm.sh
```

This will install `rvm`, which stands for "ruby version manager".

Next, install the Ruby version that we use and create a gemset for robotics:
```bash
rvm install 2.1.1
rvm gemset create firebots
rvm gemset use firebots
```

## Install npm and nodejs

```brew
brew install node
```

Aren't you glad you have homebrew?

## Clone the project

I recommend making a folder under your home directory called "robotics" and cloning this project within the robotics folder. Better to keep all the robotics stuff in one place.
```bash
# if you don't have access, contact Logan
git clone git@gitlab.com:loganh/training.git
cd training
```

## Install dependencies

```bash
# install javascript dependencies listed in package.json
npm install
# install build tools
npm install -g react-tools browserify envify clean-css

# install sass compiler & file watcher
gem install sass watchr
```

## Make dev server

```bash
make dev API_BASE=http://localhost:9977
```

You should now be able to go to `localhost:5000` in your browser and see a development version of the training site!

**Note: You will not be able to do much with the training site unless you also have the API up and running on port 9977 locally.**
