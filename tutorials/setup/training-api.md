# Dev environment for training api

Currently these instructions are for Mac OS X only. Linux users should be able to follow along pretty easily, generally replacing `brew` with `apt-get`/`yum`/etc. Both Linux and OS X have the unix-nature.

## Install homebrew
Open Terminal.app. Download and install homebrew (instructions from http://brew.sh/):
```bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Make sure that you (1) follow all instructions and (2) do
```bash
brew doctor # check for postinstall problems
brew update # get notified of new packages
brew upgrade # download and install outdated packages
```

## Install git
```bash
brew install git
```

## Install rvm

```bash
\curl -sSL https://get.rvm.io | bash -s stable
source /etc/profile.d/rvm.sh
```

This will install `rvm`, which stands for "ruby version manager".

Next, install the Ruby version that we use and create a gemset for robotics:
```bash
rvm install 2.1.1
rvm gemset create firebots
rvm gemset use firebots
```

## Clone the project

I recommend making a folder under your home directory called "robotics" and cloning this project within the robotics folder. Better to keep all the robotics stuff in one place.
```bash
# if you don't have access, contact Logan
git clone git@gitlab.com:loganh/training-api.git
cd training-api
```

## Install dependencies

```bash
gem install bundler
bundle install
```

## Create database

```bash
brew install postgresql
# MAKE SURE you follow all postinstall instructions before moving on

mkdir -p db/pg
initdb db/pg
createdb training
```

## Run migrations, start database, start cache

In another tab/window...
```bash
# run migrations (create database structure)
sequel -m migrations postgres://localhost:5432/training
postgres -D db/pg # start server
```

In yet another tab/window...
```bash
memcached # start the cache
```

## Make dev server

In your original tab/window...
```bash
thin -R config.ru -p 9977 start
```

You should now be able to go to `localhost:9977` in your browser and see a JSON message: `{"status":404,"message":"Not found!"}`.

**Note: you will have to quit and restart the server every time you make a code change to the api. This doesn't happen often so there is no automatic rebuild like there is for the web app.**
