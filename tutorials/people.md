### People to contact with questions, etc.

All of these people want to train you so they don't have to do as much work, so don't be shy. :)

All of these are level 3 or 4, meaning that they have a detailed enough understanding to teach.

- [Logan Howard](https://app.oflogan.com/user/logan)
