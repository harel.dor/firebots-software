Learn how to code in Javascript! We use Javascript for the badge and scouting website frontend.

## General Javascript
- http://www.codecademy.com/tracks/javascript

## Express
- http://code.tutsplus.com/tutorials/introduction-to-express--net-33367

## Node.js
- http://nodeschool.io/
- http://www.nodebeginner.org/
