Learn how to code in Ruby! We use Ruby for the badge and scouting website API.

- http://www.codecademy.com/tracks/ruby
- https://koans.heroku.com/en
- http://tryruby.org/levels/1/challenges/0
- https://rubymonk.com/
