# Sublime Text 2/3

Who uses this editor: Logan

## Install instructions
Get either [Sublime 2](http://www.sublimetext.com/2) or [Sublime 3](http://www.sublimetext.com/3) and install the version for your platform.

## Tutorial links

- http://code.tutsplus.com/articles/perfect-workflow-in-sublime-text-free-course--net-27293
